Irssi script for completing Matrix nicks from hy-tgbot
======================================================

# Usage

1. Put the script it in ~/.irssi/scripts/
2. In irssi, do /script load hy-tgbot_complete.pl

# Autorun

1. Put the script it in ~/.irssi/scripts/
2. cd ~/.irssi/scripts/autorun/ ; ln -s ../hy-tgbot_complete.pl .

