use strict;
use warnings;

use Irssi qw(signal_add_last signal_add_first strip_codes);

our $VERSION = '0.04';

our %IRSSI = (
	authors     => 'Aki Rehn',
	contact     => 'aki.rehn@helsinki.fi',
	name        => 'Autocomplete for hy-tgbot nicks',
	description => 'Adds Telegram nicks from hy-tgbot to completion list.',
	license     => 'Public Domain',
);

my $append_char = ':';
my @tgbot_nicks = ('hy-tgbot', 'K30TG');

my $speakers_per_channel = {};

signal_add_first 'message public' => sub {
	my ($server, $message, $speaker, $address, $target) = @_;

	# if exists, remove from order of speakers of this channel and add to top
	@{ $speakers_per_channel->{$target} } = grep { $_ ne $speaker } @{ $speakers_per_channel->{$target} || [] };
	unshift @{ $speakers_per_channel->{$target} }, $speaker;

    # not a telegram bot message, we are done
	return unless grep { $_ eq $speaker } @tgbot_nicks;

	# if message does not look like it contains a nick we are done
	return unless $message =~ /<([^>]+)>/;

	# get the telegram nick from tgbot's message
	my $tg_nick = $1;
	my $clean_nick = strip_codes($tg_nick);

	# if exists, remove from order of speakers of this channel and add to top
	@{ $speakers_per_channel->{$target} } = grep { $_ ne $clean_nick } @{ $speakers_per_channel->{$target} || [] };
	unshift @{ $speakers_per_channel->{$target} }, $clean_nick;
};

signal_add_first 'complete word' => sub {
	my ($cl, $win, $word, $start, $ws) = @_;

	# no completion list?
	return unless $cl;

	# no word to search for given
	return unless $word;

	# this is not a channel windows
	return unless $win && $win->{active} && $win->{active}{type} eq 'CHANNEL';

	# this will be appended after the nick
	my $char = $append_char;

	# do not append anything if there is something typed before completion
	$char = '' if $start;

	# nicks on the channel
	my @channel_nicks = map { $_ . $char } grep { /^\Q$word/i } map { $_->{nick} } $win->{active}->nicks();

	# recent speakers on the channel
	my $channel = $win->{active}{name};
	my @recent_speakers = map { $_ . $char } grep { /^\Q$word/i } @{ $speakers_per_channel->{$channel} || [] };

	# let's add unique entries to complete list
	push @$cl, uniq(@recent_speakers, @channel_nicks);
};

# borrowed from List::MoreUtils as it does not seem to be installed everywhere
sub uniq (@) {
	my %seen = ();
	my $k;
	my $seen_undef;
	grep { defined $_ ? not $seen{ $k = $_ }++ : not $seen_undef++ } @_;
}

